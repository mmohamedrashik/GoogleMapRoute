package guy.droid.com.routegooglemap;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.geojson.GeoJsonLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Spinner routes;
    MapView mMapView;
    GoogleMap mGoogleMap;
    ArrayList<LatLng> latLngs;
    Document document;
    LatLng fromPosition,toPosition;
    GMapV2GetRouteDirection v2GetRouteDirection;
    MarkerOptions start,end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        latLngs = new ArrayList<>();
        v2GetRouteDirection = new GMapV2GetRouteDirection();



       fromPosition = new LatLng(8.4924804,76.9113206);
        toPosition = new LatLng(8.5282645,76.9022488);
        routes = (Spinner)findViewById(R.id.routes);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if(status != ConnectionResult.SUCCESS)
        {
            GooglePlayServicesUtil.getErrorDialog(status, this, 100).show();
        }else
        {
            mMapView = (MapView) findViewById(R.id.mapView);
            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();
            mGoogleMap = mMapView.getMap();
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(8.4924804,76.9113206)).zoom(12).build();
            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            latLngs.add(new LatLng(8.4924804,76.9113206));
            latLngs.add( new LatLng(8.5282645,76.9022488));
          //  draw_routes(latLngs,Color.YELLOW);
            latLngs.clear();

            latLngs.add(new LatLng(8.4817083,76.9553193));
            latLngs.add( new LatLng(8.5099813,76.9015893));
           // draw_routes(latLngs,Color.GREEN);
            latLngs.clear();
            List<LatLng> ress = new ArrayList<>();
            try {
                GeoJsonLayer layer = new GeoJsonLayer(mGoogleMap,new JSONObject(""));
                layer.addLayerToMap();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                ress= PolyUtil.decode("wi{p@wwtwMoFnA\\tAvCpLL~Au@~CMlEMlBmAT}@N_ATq@xAErA?fBRlBKjFfBxMyAbJwD`KoK|QyCxHyDpP{EnVuCrI{e@h~@_KdRKhH_DhKmHtGeEbCeGdFoLj[aDfPsBbP_FxQmItSeFpD_LpSyCpHcAxH{N|R}MvM_FrGiQhQaOzPcHdFuNhGgDvG}BhJ{GbGoGpJ_InOeDxCcP~MoBpDsJjMeM`PcGlKoMtGoE`J{FxTqA~PaBbDKfGu@~Ih@dEvDtF]jDqBzBcQxN}CtEoQzMkUvIwMbEuM`Z{Pr[iMxPuBfFoBrKaG`KgJ~ImCrEcF~PsA~KsBnGwBnBqMvFyDdDcC~HiLfGgUxKkF`AoAlA_BrGwC~H_HxIwHb@wQpI_FlEkCzEmIlWiCnMcDlOeCjC}CtBu@zBm@jIPtEsAlEShGm@jQg@`TeAfDyOhPuGxN}AvFcJ`d@cQz_@qDtFsAnGgBhNwIja@sQhq@yLn^sCbE}FtBmCbCoA|E{B|AaB|BCzE}BvDoD~CqBd@kB~BwErEcDzM[bJu@vCyExNu@tIDtUgC~L{HxLoIdEgFvCsKxK{ExEiKfB}C`CaAtJ|A~IiFhPiLrEkBpDLtKpB~I{FfDgD|FcHzDoBhNqCzDcZnRoJrDwMtKoBnEaCdAeFZaAz@qCzDcFvGuEnHaUfLoH|EkJ]iDfCwL|RwOpPiMxI}FvJuEhJsGxDcDjDw@vCmCbC}Ef@uIbReRdN}PdO}FxQqElNg@rKNtSt@rMgBdQwG|GaDhG}EfMuChDoH|AeFjAmDWoCdBwC`LyCnE_Db@yGZ_C`DcL`PyHlGuJlMgDzHk@zAHbQ_AvHoDnGL`EOtAcClCsCfEFhOcAvNaKlMuTlLqBrEsF|`@qMlIyFlCkAbJmJpKqFzKoCfCwB`EEdBi@lS{H`UgHvQmJrNmKhNwGbQiDvHqFfEsFz@eOvVoE~DcKvEeKzKcFdB_BV_CdCgArCaB`IiV~b@kEjJiOzNsFnHmIxNmGjLkKbMoKvLyGpLeKvVwKtSeQ`UiPxU{I|OoIp@iDn@aBxAuFhSj@pDxArBn@`DDnKoBf\\?tAlGbGrBpD\\tQUpO~@`Lj@hPl@bSMlHb@xKe@fEeCjGiAjFb@\\LL?PsEzCcb@r`@{XtVcHbHic@ra@cXtWgXvYiKzNgLvNqTvRyt@`o@eGtFyAzAq@?eFhFiAz@e@WuAmBEsEQwBy@o@s@@C{@d@yBZ_FcBuA{@wE`@uDHuBz@cBvE_A\\DZqAa@uLEmBeLyAuBaA");
            }catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
            }
            Log.wtf("------------",ress+"");

            draw_routes((ArrayList<LatLng>) ress,Color.RED);
            start = new MarkerOptions().position(ress.get(0)).title("START");
            end = new MarkerOptions().position(ress.get(ress.size() - 1)).title("END");
            mGoogleMap.addMarker(start);
            mGoogleMap.addMarker(end);

            String response = new String("{\n" +
                    "  \"geocoded_waypoints\": [\n" +
                    "    {\n" +
                    "      \"geocoder_status\": \"OK\",\n" +
                    "      \"place_id\": \"ChIJaZ6dqXC8BTsRyD70jRoykp4\",\n" +
                    "      \"types\": [\n" +
                    "        \"route\"\n" +
                    "      ]\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"geocoder_status\": \"OK\",\n" +
                    "      \"place_id\": \"ChIJR827Bbi7BTsRy4FcXKufQxU\",\n" +
                    "      \"types\": [\n" +
                    "        \"locality\",\n" +
                    "        \"political\"\n" +
                    "      ]\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"routes\": [\n" +
                    "    {\n" +
                    "      \"bounds\": {\n" +
                    "        \"northeast\": {\n" +
                    "          \"lat\": 8.528309399999999,\n" +
                    "          \"lng\": 76.9227303\n" +
                    "        },\n" +
                    "        \"southwest\": {\n" +
                    "          \"lat\": 8.488467,\n" +
                    "          \"lng\": 76.88986799999999\n" +
                    "        }\n" +
                    "      },\n" +
                    "      \"copyrights\": \"Map data ©2016 Google\",\n" +
                    "      \"legs\": [\n" +
                    "        {\n" +
                    "          \"distance\": {\n" +
                    "            \"text\": \"8.8 km\",\n" +
                    "            \"value\": 8827\n" +
                    "          },\n" +
                    "          \"duration\": {\n" +
                    "            \"text\": \"17 mins\",\n" +
                    "            \"value\": 1040\n" +
                    "          },\n" +
                    "          \"end_address\": \"Thiruvananthapuram, Kerala 695001, India\",\n" +
                    "          \"end_location\": {\n" +
                    "            \"lat\": 8.528309399999999,\n" +
                    "            \"lng\": 76.9022021\n" +
                    "          },\n" +
                    "          \"start_address\": \"Chilakkoor - Vallakkadavu Rd, Chacka, Thiruvananthapuram, Kerala 695008, India\",\n" +
                    "          \"start_location\": {\n" +
                    "            \"lat\": 8.4927054,\n" +
                    "            \"lng\": 76.91157260000001\n" +
                    "          },\n" +
                    "          \"steps\": [\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"0.8 km\",\n" +
                    "                \"value\": 770\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"1 min\",\n" +
                    "                \"value\": 61\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.490583299999999,\n" +
                    "                \"lng\": 76.91669759999999\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Head <b>southeast</b> on <b>Chilakkoor - Vallakkadavu Rd</b>/<b>Veli - Perumathura Rd</b><div style=\\\"font-size:0.9em\\\">Pass by Chacka Fire and Rescue Station (on the left in 500&nbsp;m)</div>\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"mvyr@ixltMlBcB|DoDn@k@^[POp@o@@CpD}DLMDGFKDMBK?C@G@I?GAI?GAEAECGCGYa@a@k@gAyASMkAqAAEAC?C@C?A\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.4927054,\n" +
                    "                \"lng\": 76.91157260000001\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"0.9 km\",\n" +
                    "                \"value\": 925\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"2 mins\",\n" +
                    "                \"value\": 102\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.491958199999999,\n" +
                    "                \"lng\": 76.92272609999999\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Turn <b>right</b> to stay on <b>Chilakkoor - Vallakkadavu Rd</b>/<b>Veli - Perumathura Rd</b><div style=\\\"font-size:0.9em\\\">Pass by Ilampala Sree Bhagavathy Temple (on the left in 800&nbsp;m)</div>\",\n" +
                    "              \"maneuver\": \"turn-right\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"ciyr@kxmtM?ABEBEBCBCDGJIv@q@b@a@\\\\]l@c@v@s@dA}@LKp@m@LMBC@E@A@CBG@E?C@E?CAC?A?CCEEEGIm@a@s@s@_C_Cy@w@g@_@cAs@QMQMGEGGQQo@u@SOIKWYOSOQMQGKEKe@eBGSEOAEAC?C\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.490583299999999,\n" +
                    "                \"lng\": 76.91669759999999\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"5.0 km\",\n" +
                    "                \"value\": 4954\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"8 mins\",\n" +
                    "                \"value\": 488\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.5237488,\n" +
                    "                \"lng\": 76.89134539999999\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"At the roundabout, take the <b>1st</b> exit onto <b>NH66</b><div style=\\\"font-size:0.9em\\\">Partial toll road</div><div style=\\\"font-size:0.9em\\\">Pass by Swami Ayyappa Temple (on the left in 1.0&nbsp;km)</div>\",\n" +
                    "              \"maneuver\": \"roundabout-left\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"wqyr@a~ntMA@A?A?AAA?A?c@Zw@l@A?C@A?E@WXe@d@u@p@gA`AYVaA|@mAhAqAnAa@b@a@`@}@z@i@j@kAjAkCjCc@b@{@|@mBnBKJyArAYXIFGHqAdAUTuBvBa@`@OLUT{A`Bu@v@EBcB`B{BzB_@\\\\gAfA_DhD{@`AuCrC}EhFw@bAw@bAkBfCY`@eA`BSXGFEFaBxBy@jAqAhBoAbBgArAkAtAKTaAbAWVKLSPwAtASLaB|AmE|Dq@n@_DnCo@d@kAhA{ClCgBvAGNkChCuDbDKHeBtAm@d@qEnDmCvBiA~@cAx@sG|FaAz@{@p@w@r@sAjAs@n@eAdAaAdAWT\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.491958199999999,\n" +
                    "                \"lng\": 76.92272609999999\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"17 m\",\n" +
                    "                \"value\": 17\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"1 min\",\n" +
                    "                \"value\": 10\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.5238616,\n" +
                    "                \"lng\": 76.89145379999999\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Turn <b>right</b> at <b>Kuzhivila Junction</b> onto <b>Ulloor - Akkulam Rd</b>\",\n" +
                    "              \"maneuver\": \"turn-right\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"mx_s@}yhtMUS\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.5237488,\n" +
                    "                \"lng\": 76.89134539999999\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"0.3 km\",\n" +
                    "                \"value\": 257\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"1 min\",\n" +
                    "                \"value\": 23\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.525547999999999,\n" +
                    "                \"lng\": 76.88986799999999\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Turn <b>left</b>\",\n" +
                    "              \"maneuver\": \"turn-left\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"cy_s@qzhtM[RWVgAtAm@l@wAlAk@h@KFIDGBE@\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.5238616,\n" +
                    "                \"lng\": 76.89145379999999\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"0.4 km\",\n" +
                    "                \"value\": 369\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"1 min\",\n" +
                    "                \"value\": 66\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.526798599999999,\n" +
                    "                \"lng\": 76.8923774\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Turn <b>right</b>\",\n" +
                    "              \"maneuver\": \"turn-right\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"uc`s@uphtMIMCCAACACACAC?CACAECCCEGAEEIu@iAOWIQAKCO@ORyA@K@W?a@CQEWGk@C]UISEKAO@ODQD\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.525547999999999,\n" +
                    "                \"lng\": 76.88986799999999\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"0.9 km\",\n" +
                    "                \"value\": 859\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"3 mins\",\n" +
                    "                \"value\": 180\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.525411099999999,\n" +
                    "                \"lng\": 76.89822149999999\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Turn <b>right</b>\",\n" +
                    "              \"maneuver\": \"turn-right\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"ok`s@k`itMAKAKAQ?S?IBIFSFY@Q@MDQBGBG@G?KC[CUASAO@O?OBMDMHMJSBI?E?C?CAAA?EAI?KAGCGEGGGKAAKMCEEGEGCEAEAK?OAWAKCMMc@ESEIEGKGECEEAC?G@MDSDMFMBEBG@A?A?CAEACCECCAI?E@IJu@H_@DU?I@]@w@@k@@c@f@KNGXONILGREd@KZEr@GT@L@NBZH\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.526798599999999,\n" +
                    "                \"lng\": 76.8923774\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"0.3 km\",\n" +
                    "                \"value\": 303\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"1 min\",\n" +
                    "                \"value\": 37\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.5255615,\n" +
                    "                \"lng\": 76.9009681\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Turn <b>left</b> onto <b>Ulloor - Akkulam Rd</b><div style=\\\"font-size:0.9em\\\">Pass by National Institute of Speech and Hearing (on the left)</div>\",\n" +
                    "              \"maneuver\": \"turn-left\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"yb`s@{djtM?{A?Q[{IC{@AK@G?E@E\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.525411099999999,\n" +
                    "                \"lng\": 76.89822149999999\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "              \"distance\": {\n" +
                    "                \"text\": \"0.4 km\",\n" +
                    "                \"value\": 373\n" +
                    "              },\n" +
                    "              \"duration\": {\n" +
                    "                \"text\": \"1 min\",\n" +
                    "                \"value\": 73\n" +
                    "              },\n" +
                    "              \"end_location\": {\n" +
                    "                \"lat\": 8.528309399999999,\n" +
                    "                \"lng\": 76.9022021\n" +
                    "              },\n" +
                    "              \"html_instructions\": \"Slight <b>left</b><div style=\\\"font-size:0.9em\\\">Pass by PTC Buildings (on the right)</div><div style=\\\"font-size:0.9em\\\">Destination will be on the right</div>\",\n" +
                    "              \"maneuver\": \"turn-slight-left\",\n" +
                    "              \"polyline\": {\n" +
                    "                \"points\": \"wc`s@avjtMCOAKAQAS?WAIAC?CACCAEAGCIA_CKaAG_AOA?eAKUCSAOCOEOCOGMEOKKGMIKIAA\"\n" +
                    "              },\n" +
                    "              \"start_location\": {\n" +
                    "                \"lat\": 8.5255615,\n" +
                    "                \"lng\": 76.9009681\n" +
                    "              },\n" +
                    "              \"travel_mode\": \"DRIVING\"\n" +
                    "            }\n" +
                    "          ],\n" +
                    "          \"traffic_speed_entry\": [],\n" +
                    "          \"via_waypoint\": []\n" +
                    "        }\n" +
                    "      ],\n" +
                    "      \"overview_polyline\": {\n" +
                    "        \"points\": \"mvyr@ixltMzJ{IbA_ArDaERULYFa@C_@EM]i@iBeCSMkAqACI@GFOXYxBqBdBwArAiAdAeAHS?QIQu@k@sDsDaBwAuAaAs@m@eBkBu@cAy@uCEKC?EAaBjAG@}@~@yEhEcF~E_I~HyE|EeC~BgBzAuBvBq@n@qBvB{GxGgBdB{EjFuCrC}EhFoBfCeChDyAzBiDtEaDlEsChDKTaAbAc@d@kBfBSLaB|A_GlFoEtDgFvEgBvAGNkChCuDbDqB~AwMlKwIvH}BlBkC~ByBtByAzAUS[R_BlBeCzBw@p@QHE@IMEEOEQKcAaBYi@E[TiBBc@Cs@McAC]UI_@G_@FQDAKC]?]J]Hk@F_@FO@SGq@Cc@@_@H[Ta@BWSCSEOMYa@Q[A[Cc@]oAQOKIAKFa@Ti@?MKW@OTuAD_@BuABoAv@Sh@Y`@M`AQr@GT@\\\\DZH?{A[mJEgA@MAUC]Ak@EU[IaESaAO_CU}@Ww@i@\"\n" +
                    "      },\n" +
                    "      \"summary\": \"NH66\",\n" +
                    "      \"warnings\": [],\n" +
                    "      \"waypoint_order\": []\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"status\": \"OK\"\n" +
                    "}");

            try {
                JSONObject json = new JSONObject(response);
               // Toast.makeText(getApplicationContext(),json.toString()+"",Toast.LENGTH_LONG).show();
               String s= json.getString("routes");
                JSONArray array = new JSONArray(s);

                Toast.makeText(getApplicationContext(),array.length()+"",Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),e+"",Toast.LENGTH_LONG).show();
            }

            // Toast.makeText(getApplicationContext(),re+"",Toast.LENGTH_LONG).show();
           // Log.wtf("---",re+"");
            // GetRouteTask getRoute = new GetRouteTask();
            // startService(new Intent(MainActivity.this, GSTracker.class));
          //  getRoute.execute();
           // fromPosition = new LatLng(8.4924804,76.9113206);
          //  toPosition = new LatLng(8.5282645,76.9022488);

           // GetRouteTask getRoutes = new GetRouteTask();
            // startService(new Intent(MainActivity.this, GSTracker.class));
           // getRoutes.execute();
           // fromPosition = new LatLng(8.5282645,76.9022488);
         //   toPosition = new LatLng(8.510726, 76.962393);
          //  new GetRouteTask().execute();



        }
    }
    private class GetRouteTask extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog;
        String response = "";
        @Override
        protected void onPreExecute() {
            Dialog = new ProgressDialog(MainActivity.this);
            Dialog.setMessage("Loading route...");
            Dialog.show();


        }

        @Override
        protected String doInBackground(String... urls) {
            //Get All Route values
            try {
                document = v2GetRouteDirection.getDocument(fromPosition, toPosition, GMapV2GetRouteDirection.MODE_DRIVING);
                response = "Success";
            }catch (Exception e)
            {
                System.out.print("ERROR"+e.toString());
            }

            return response;

        }

        @Override
        protected void onPostExecute(String result) {
        //    mGoogleMap.clear();
            // startService(new Intent(MainActivity.this, GSTracker.class));
            Log.w("ERR","______________"+result);
            if(result.equalsIgnoreCase("Success")){
                ArrayList<LatLng> directionPoint = v2GetRouteDirection.getDirection(document);
                PolylineOptions rectLine = new PolylineOptions().width(10).color(
                        Color.BLUE);


                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }

               // rectLine.add(new LatLng(8.510726, 76.962393));
                // Adding route on the map
                mGoogleMap.addPolyline(rectLine);
                Dialog.cancel();



           }


        }


    }
    public void draw_routes(ArrayList<LatLng> lists,int color)
    {
        ArrayList<LatLng> directionPoint = lists;
        PolylineOptions rectLine = new PolylineOptions().width(5).color(
                color);
        for (int i = 0; i < directionPoint.size(); i++) {
            rectLine.add(directionPoint.get(i));
        }
        mGoogleMap.addPolyline(rectLine);

    }

}
